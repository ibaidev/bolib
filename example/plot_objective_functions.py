# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of BOlib.
#
#    BOlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BOlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BOlib. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import matplotlib.pyplot as plt
from matplotlib import pyplot, cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

import bolib

plt.style.use('ggplot')


def plot_3d(objective_function, length=20):
    """
    Plot 3D functions

    :param objective_function:
    :type objective_function:
    :param length:
    :type length:
    :return:
    :rtype:
    """
    bounds = objective_function.get_bounds()

    if len(bounds) != 2:
        return

    x_grid = np.linspace(bounds[0][0], bounds[0][1], length)
    y_grid = np.linspace(bounds[1][0], bounds[1][1], length)
    x_grid, y_grid = np.meshgrid(x_grid, y_grid)
    grid = np.vstack((x_grid.flatten(), y_grid.flatten())).T
    z_points = objective_function.evaluate(grid)
    z_points = z_points.reshape(length, length)

    fig = pyplot.figure()
    axis = fig.add_subplot(projection='3d')

    surf = axis.plot_surface(x_grid, y_grid,
                             z_points, rstride=1, cstride=1,
                             cmap=cm.cool, linewidth=0, antialiased=False,
                             alpha=0.3)
    axis.contour(x_grid.tolist(), y_grid.tolist(), z_points.tolist(),
                 zdir='z', offset=z_points.min(), cmap=cm.cool)

    axis.set_xlim(bounds[0][0], bounds[0][1])
    axis.set_ylim(bounds[1][0], bounds[1][1])
    pyplot.title(objective_function.__class__.__name__)
    axis.zaxis.set_major_locator(LinearLocator(10))
    axis.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    fig.colorbar(surf, shrink=0.5, aspect=5)

    pyplot.show()


if __name__ == '__main__':
    OFS = [
        bolib.ofs.Branin,
        bolib.ofs.Camelback,
        bolib.ofs.Hartmann,
        bolib.ofs.Rastrigin,
        bolib.ofs.Rosenbrock,
        bolib.ofs.Schwefel,
        bolib.ofs.Sphere
    ]

    for objective_function_class in OFS:
        objective_function = objective_function_class()
        plot_3d(objective_function)
