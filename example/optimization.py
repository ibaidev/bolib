# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of BOlib.
#
#    BOlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BOlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BOlib. If not, see <http://www.gnu.org/licenses/>.

import bolib

import gplib


def optimize_example():
    """

    :return:
    :rtype:
    """

    of = bolib.ofs.Branin()

    of.evaluate([1.0, 1.0])  # 27.702905548512433

    model = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.SquaredExponential(ls=([1.] * of.d))
    )

    metric = gplib.me.LML()

    fitting_method = gplib.fit.MultiStart(
        obj_fun=metric.fold_measure,
        max_fun_call=300,
        nested_fit_method=gplib.fit.LocalSearch(
            obj_fun=metric.fold_measure,
            max_fun_call=75,
            method='Powell'
        )
    )

    validation = gplib.dm.Full()

    af = bolib.afs.ExpectedImprovement()

    # We get a random sample within the bounds of the objective function
    bo = bolib.methods.BayesianOptimization(
        model, fitting_method, validation, af
    )

    bo.set_seed(seed=1)

    x0 = bo.random_sample(of.get_bounds(), batch_size=10)

    bo.minimize(
        of.evaluate, x0,
        bounds=of.get_bounds(),
        tol=1e-5,
        maxiter=of.get_max_eval(),
        disp=True
    )


if __name__ == '__main__':
    optimize_example()
