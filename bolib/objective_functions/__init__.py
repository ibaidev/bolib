# TODO of_gp
# TODO svm_on_grid
# TODO lda_on_grid
from .branin import Branin
from .camelback import Camelback
from .hartmann import Hartmann
from .rastrigin import Rastrigin
from .rosenbrock import Rosenbrock
from .schwefel import Schwefel
from .sphere import Sphere
