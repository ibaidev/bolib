FROM ubuntu:xenial

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    python3-tk \
 && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash pyuser

USER pyuser

WORKDIR /app

RUN python3 -m pip install --upgrade pip

RUN python3 -m pip install bolib

ENTRYPOINT ["python3"]
