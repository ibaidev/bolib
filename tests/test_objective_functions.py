# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of BOlib.
#
#    BOlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BOlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BOlib. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import unittest
import bolib


class OFTest(unittest.TestCase):
    """ Kernel test set """


def of_test_generator(of_class):
    """ Generate a test per each OF """
    of = of_class()

    def test_of(self):
        """ Test optimun """
        result = of.evaluate(of.get_objective())
        np.testing.assert_allclose(result,
                                   of.get_objective_val(),
                                   atol=1e-1)

    return test_of


for of in [
            bolib.ofs.Branin,
            bolib.ofs.Camelback,
            bolib.ofs.Hartmann,
            bolib.ofs.Rastrigin,
            bolib.ofs.Rosenbrock,
            bolib.ofs.Schwefel,
            bolib.ofs.Sphere
        ]:
    test_of = of_test_generator(of)
    setattr(OFTest,
            'test_%s' % of.__name__,
            test_of)
