# -*- coding: utf-8 -*-
#
#    Copyright 2019 Ibai Roman
#
#    This file is part of BOlib.
#
#    BOlib is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BOlib is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BOlib. If not, see <http://www.gnu.org/licenses/>.

import unittest

import numpy as np
import scipy.optimize as spo

import gplib

import bolib


class MethodTest(unittest.TestCase):
    """ Kernel test set """


def method_test_generator(af_class, of_class, seed):
    """
    Generate a test per each method

    :param af_class:
    :type af_class:
    :param of_class:
    :type of_class:
    :param seed:
    :type seed:
    :return:
    :rtype:
    """
    of = of_class()

    model = gplib.GP(
        mean_function=gplib.mea.Fixed(),
        covariance_function=gplib.ker.SquaredExponential(ls=([1.] * of.d))
    )

    metric = gplib.me.LML()

    fitting_method = gplib.fit.MultiStart(
        obj_fun=metric.fold_measure,
        max_fun_call=300,
        nested_fit_method=gplib.fit.LocalSearch(
            obj_fun=metric.fold_measure,
            max_fun_call=75,
            method='Powell'
        )
    )

    validation = gplib.dm.Full()

    af = af_class()

    bo = bolib.methods.BayesianOptimization(
        model, fitting_method, validation, af
    )

    def test_method(self):
        """ Test optimun """
        bo.set_seed(seed=seed)

        x0 = bo.random_sample(of.get_bounds(), batch_size=10)

        result = spo.minimize(
            of.evaluate,
            x0,
            bounds=of.get_bounds(),
            method=bo.minimize,
            tol=1e-5,
            options={
                'maxiter': int(of.get_max_eval() * 0.35),
                'disp': False
            }
        )
        result = result.fun

        np.testing.assert_allclose(
            result,
            of.get_objective_val(),
            rtol=3e-2
        )

    return test_method


seed = 1

for af in [
            bolib.afs.ProbabilityOfImprovement,
            bolib.afs.ExpectedImprovement,
            bolib.afs.UpperConfidenceBound
        ]:
    for of in [
                bolib.ofs.Branin,
                bolib.ofs.Camelback,
                bolib.ofs.Rastrigin,
                bolib.ofs.Rosenbrock,
                bolib.ofs.Schwefel,
                bolib.ofs.Sphere
            ]:
        test_method = method_test_generator(af, of, seed)
        setattr(MethodTest,
                'test_%s_%s' % (af.__name__, of.__name__),
                test_method)
