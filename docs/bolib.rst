bolib package
=============

Subpackages
-----------

.. toctree::

   bolib.acquisition_functions
   bolib.objective_functions
   bolib.optimization_methods

Module contents
---------------

.. automodule:: bolib
   :members:
   :undoc-members:
   :show-inheritance:
