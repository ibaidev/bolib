bolib.optimization\_methods package
===================================

Submodules
----------

bolib.optimization\_methods.bayesian\_optimization module
---------------------------------------------------------

.. automodule:: bolib.optimization_methods.bayesian_optimization
   :members:
   :undoc-members:
   :show-inheritance:

bolib.optimization\_methods.optimization\_method module
-------------------------------------------------------

.. automodule:: bolib.optimization_methods.optimization_method
   :members:
   :undoc-members:
   :show-inheritance:

bolib.optimization\_methods.random\_grid module
-----------------------------------------------

.. automodule:: bolib.optimization_methods.random_grid
   :members:
   :undoc-members:
   :show-inheritance:

bolib.optimization\_methods.sequential\_optimization module
-----------------------------------------------------------

.. automodule:: bolib.optimization_methods.sequential_optimization
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: bolib.optimization_methods
   :members:
   :undoc-members:
   :show-inheritance:
