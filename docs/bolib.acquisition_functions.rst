bolib.acquisition\_functions package
====================================

Submodules
----------

bolib.acquisition\_functions.acquisition\_function module
---------------------------------------------------------

.. automodule:: bolib.acquisition_functions.acquisition_function
   :members:
   :undoc-members:
   :show-inheritance:

bolib.acquisition\_functions.expected\_improvement module
---------------------------------------------------------

.. automodule:: bolib.acquisition_functions.expected_improvement
   :members:
   :undoc-members:
   :show-inheritance:

bolib.acquisition\_functions.probability\_of\_improvement module
----------------------------------------------------------------

.. automodule:: bolib.acquisition_functions.probability_of_improvement
   :members:
   :undoc-members:
   :show-inheritance:

bolib.acquisition\_functions.upper\_confidence\_bound module
------------------------------------------------------------

.. automodule:: bolib.acquisition_functions.upper_confidence_bound
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: bolib.acquisition_functions
   :members:
   :undoc-members:
   :show-inheritance:
